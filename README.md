## Getting Started ##

For getting started please register at admenu.in. Fields needed to be filled during registration are as below.
*  Company Name / Entity Name.
*  Contact Person. 
*  SkypeId.
*  Postback required for server to server integration, please read the postback section for more info.

## Request ##

### Host url ###

http://api.admenu.in/getOffers

### Parameters ###
<table>
<th>Parameter</th>
<th>Type</th>
<th>Mandatory</th>
<th>Example</th>
<th>Description</th>
<tr>
<td>pub</td>
<td>string</td>
<td>Yes</td>
<td>pub=5ad6f6a510772dd0f86b4fkn31</td>
<td>Publisher's unique id , can be generated by registering at http://admenu.in/register</td>
</tr>
<tr>
<td>_sort</td>
<td>string</td>
<td>No</td>
<td>_sort=payout:asc or _sort=payout</td>
<td>
Response of api can be sorted on the basis of any key in the app data.
*  Ascending example: _sort=size:asc or _sort=size
*  Descending example: _sort=size:desc or _sort=-size
</td>
</tr>
<tr>
<td>_limit</td>
<td>string</td>
<td>No</td>
<td>_limit=30</td>
<td>
Limits app list to the given value. By default app list is limited to 500
</td>
</tr>
<tr>
<td>_start</td>
<td>string</td>
<td>No</td>
<td>_start=10&_limit=30</td>
<td>
Can only be used in conjunction with _limit. Skips given number of records from starting. 
</td>
</tr>
</table>


## Response ##

<table>
<tr>
<th>
Parameter
</th>
<th>
Description
</th>
<th>
Type
</th>
</tr>
<tr>
<td>
id
</td>
<td>
The id of the offer.	
</td>
<td>
string
</td>
</tr>
<tr>
<td>
name
</td>
<td>
The name of the offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td>
package
</td>
<td>
The package name of the offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td>
desc
</td>
<td>
A brief description of offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td> 
payout
</td>
<td>
The revenue(USD) of the offer.
</td>
<td>
double
</td>
</tr>
<tr>
<td> 
cap
</td>
<td>
The maximum conversion allowed of the offer for all publishers	
</td>
<td>
int
</td>
</tr>
<tr>
<td> 
trackinglink
</td>
<td>
Link of the offer. It will be used for tracking conversion. 
</td>
<td>
string
</td>
</tr>
<tr>
<td>
country
</td>
<td>
The target countries of the offer(empty array means global).
</td>
<td>
array
</td>
</tr>
<tr>
<td> 
os
</td>
<td>
Operating system on which the offer is available. Majorly android or ios, if empty then offer is os agnostic.
</td>
<td>
string
</td>
</tr>
<tr>
<td> 
traffic 
</td>
<td>
Type of traffic(incentive based or non incentive based) accepted for offer, incorrect traffic may lead into rejection of conversions. 
</td>
<td>
string
</td>
</tr>
<tr>
<td>
os_version 
</td>
<td>
Minimum os version required by for offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td>
carrier 
</td>
<td>
The target carrier( or telecom/broadband operator) of offer
</td>
<td>
array
</td>
</tr>
<tr>
<td>
device 
</td>
<td>
The target hardware brand of the offer.	
</td>
<td>
array
</td>
</tr>
<tr>
<td>
nettype 
</td>
<td>
The target net type of the offer(2g、2.5g、3g、4g、wifi).	
</td>
<td>
array
</td>
</tr>
<tr>
<td>
preview_url 
</td>
<td>
The preview url of the offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td>
icon_url
</td>
<td>
The link to the icon of the offer.
</td>
<td>
string
</td>
</tr>
<tr>
<td>
creative
</td>
<td>
The image materials of the offer.
</td>
<td>
array
</td>
</tr>

<tr>
<td> 
video
</td>
<td>
The video materials of the offer.
</td>
<td>
array
</td>
</tr>

<tr>
<td> 
store_label
</td>
<td>
The store (AppStore/GooglePlay) label of the offer.	
</td>
<td>
string
</td>
</tr>

<tr>
<td> 
store_rating
</td>
<td>
The store (AppStore/GooglePlay) rating of the offer.
</td>
<td>
string
</td>
</tr>

<tr>
<td> 
size
</td>
<td>
The size of the package.
</td>
<td>
string
</td>
</tr>

<tr>
<td> 
conversion_flow
</td>
<td>
Publisher can get a conversion only if a user completing this convertion flow.	
</td>
<td>
string
</td>
</tr>

<tr>
<td> 
payout_type
</td>
<td>
CPI: This means the offer is from an app store.
CPA: This means user will be redirected to a web task.
</td>
<td>
string
</td>
</tr>

<tr>
<td> 
mandatory_device
</td>
<td>
Publishers need to transmit these device parameters if it's marked as "true". 
"imei" stands for user's device IMEI 
"mac" stands for the mac address of user's device. 
"andid" stands for the android_id of user's device. 
"gaid" stands for the Google Advertising ID of user's device. 
"idfa" stands for the idfa of user's iOS device. 
</td>
<td>
array
</td>
</tr>

<tr>
<td> 
subsource_blacklist
</td>
<td>
Subsource that was blocked by the advertiser, maybe due to bad quality.
</td>
<td>
array
</td>
</tr>

<tr>
<td> 
subsource_whitelist
</td>
<td>
This offer was only opened to these subsources of you. Publisher should make sure that you will just target these subsources
</td>
<td>
array
</td>
</tr>

<tr>
<td> 
category
</td>
<td>
The category of the offer(APP、ADULT、SMARTLINK、SUBSCRIPTION).
</td>
<td>
string
</td>
</tr>

</table>

### Example ###
```json
 {
        "source": "api",
        "subsource_whitelist": [],
        "subsource_blacklist": [],
        "mandatory_identifiers": null,
        "payout_type": "CPI",
        "conversion_flow": "RR 30%\r\nSign(need):TJYJLDWGDWubxREBbhB8IdckQ7tPFvGt",
        "size": "",
        "store_rating": null,
        "store_label": [
            "Game"
        ],
        "category": "APP",
        "video": [],
        "creative": [
            {
                "url": "https://lh3.googleusercontent.com/LjjWisSoxOZHgdwRT9EEUuIjFG4GHdwhKPJjwAE9SYHkdZ6PS3tNyzHqfH1wpwD9-w",
                "mime": "image/jpeg",
                "width": 512,
                "height": 250
            },
            {
                "url": "https://lh3.googleusercontent.com/0vra1gRarmcR_hXtEkal0zAmFMEynPFJj0cxEcsIMI_acXqYRId5RQpyg8P1nIhgwPS1",
                "mime": "image/jpeg",
                "width": 512,
                "height": 384
            },
            {
                "url": "https://lh3.googleusercontent.com/hofXEQv8Wy2O-DH5eOVOhSbr0eT2can4IkSzwaak2SkyQ4NFxwfkEkX9vbgaPZkzVdc",
                "mime": "image/jpeg",
                "width": 512,
                "height": 384
            },
            {
                "url": "https://lh3.googleusercontent.com/zJJqUsrWeDJYsIJQQlMg3rRMt-RID6_AH1qeNqsmvI1Y0Fo4H6jQk-5D-1xx8UaJ73E",
                "mime": "image/jpeg",
                "width": 512,
                "height": 288
            },
            {
                "url": "https://lh3.googleusercontent.com/mAOaqOaqDTjGPA8HCJD6Jyt6YRfJkeluZDY759HcyogPTGfBs00N5bOkrWVtsnHrSAE",
                "mime": "image/jpeg",
                "width": 512,
                "height": 341
            },
            {
                "url": "https://lh3.googleusercontent.com/dSeo7DktAsFBr6zqXcDhTgc4JaiAZ772ttuSf9FbRbTniYWdMxC4AWzrkpGvPbWu724",
                "mime": "image/jpeg",
                "width": 512,
                "height": 341
            },
            {
                "url": "https://lh3.googleusercontent.com/UDwInbQYZ-mCVlHpG2-BfBFcAq2n0tLHjD8aLXUYij1j4ab3LI2H0Q0neZwm9zsKvwc",
                "mime": "image/jpeg",
                "width": 512,
                "height": 341
            },
            {
                "url": "https://lh3.googleusercontent.com/SSmBqTUpj8zgmWq3CYGTe_WN1U0W761mPsqEdEonNb0Giod9Co7MEwix50_V5g7PHeM",
                "mime": "image/jpeg",
                "width": 512,
                "height": 341
            }
        ],
        "icon_url": "https://lh3.googleusercontent.com/7jhkmUi9oFNL8b4zGj7ZD_pBJ3qpCvoJPF-p4nUhdasHyBCg67HiPoiUWUN_z_BYUoY=w96",
        "preview_url": "https://play.google.com/store/apps/details?id=com.nexon.dynastywarriors",
        "nettype": [],
        "device": [],
        "carrier": [],
        "os_version": "",
        "traffic": "non-incentive",
        "os": [
            "android"
        ],
        "country": [
            "KR"
        ],
        "trackinglink": "http://api.admenu.in/offer/?api=5ad6f6a510772dd0f86b4f51&offerId=5ad9c836ea69dd8c73c0bb17&source=0&pub=5ad6f61010772dd0f86b4f4f&subpub=5ad6f64a10772dd0f86b4f50",
        "cap": 0,
        "point": 0,
        "payout": 0.805,
        "task": "",
        "desc": "▶ 1st Anniversary collaboration with Dynasty Warriors 9! - Experience Dynasty Warriors: Unleashed with even more content and features!  ▶ Special Anniversary Event  - A grand, collaborative celebration with new game modes and tons of rewards!  ▶ Play The Northern Expedition: Battle of Mount Dingjun! - Participate in one of the most thrilling military campaigns in history!  The Dynasty Warriors: Unleashed developer team thanks all players for their participation and support throughout the game&#39;s first year. We strive to meet your expectations and look forward to earning Unleashed a place in the Musou pantheon.  Dynasty Warriors is the legendary ultimate high adrenaline battle experience in Action Combat games.  Celebrate the return of your favorite heroic characters, massive battles overflowing with onslaughts of relentless enemies wrapped in a timeless epic saga to unite the three kingdoms.   Your elite warriors and cunning strategies are forces to be reckoned with.  Promote your generals, build your armi",
        "package": "com.nexon.dynastywarriors",
        "name": "Dynasty Warriors: Unleashed",
        "id": "5ad9c836ea69dd8c73c0bb17"
    }
```


### Tracking Link ###

####Example:####
http://api.admenu.in/offer/?api=your_API_key&offerId=ABC&source={your_pub_id}&pub=your_pub_id&transaction_id={{your_click_id}}&imei={{imei}}&app_id={{app_id}}&app_name={{app_name}}&gaid={{gaid}}&device_id={{device_id}}&isp={{isp}}&os={{os}}&aff_sub1={{aff_sub1}}&aff_sub2={{aff_sub2}}&aff_sub3={{aff_sub3}} 

####Method####
GET
####Parameter####
<table>
<th>
Pramater
</th>
<th>
Type
</th>
<th>
Description
</th>
<tr>
<td>
transactionid
</td>
<td>
string
</td>
<td>
Unique Id for transaction(i.e click or userid)
</td>
</tr>
<tr>
<td>source</td>
<td>
string
</td>
<td>For your <code>sub_source_id</code> or <code>placement_id</code>(less than 150 chars)</td>
</tr>
<tr>
<td>imei</td>
<td>Publisher must transmit user's IMEI in trackinglink, if it's "true" in "mandatory_device"</td>
<td>
string
</td>
</tr>
<tr>
<td>mac</td>
<td>
string
</td>
<td>Publisher must transmit user's MAC Address in trackinglink, if it's "true" in "mandatory_device"</td>
</tr>
<tr>
<td>andid</td>
<td>
string
</td>
<td>Publisher must transmit user's Android ID in trackinglink, if it's "true" in "mandatory_device"</td>
</tr>
<tr>
<td>gaid</td>
<td>
string
</td>
<td>Publisher must transmit user's Google Advertising ID(GAID) in trackinglink, if it's "true" in "mandatory_device"</td>
</tr>
<tr>
<td>idfa</td>
<td>
string
</td>
<td>Publisher must transmit user's IDFA in trackinglink, if it's "true" in "mandatory_device"</td>
</tr>
<tr>
<td>aff_sub1</td>
<td>
string
</td>
<td>For your custom parameter</td>
</tr>
<tr>
<td>aff_sub2</td>
<td>
string
</td>
<td>For your custom parameter</td>
</tr>
<tr>
<td>aff_sub3</td>
<td>
string
</td>
<td>For your custom parameter</td>
</tr>
</table>


**PostBack:**



Please send an email at paramveer.singh@tarsan.in for getting started with our server to server api. Alternatively you can also ping us on skype at abhinav_iitr1